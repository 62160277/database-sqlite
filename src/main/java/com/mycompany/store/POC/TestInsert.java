/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.store.POC;

import Database.database;
import Model.Product;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Acer
 */
public class TestInsert {
    public static void main(String[] args) {
        Connection cnn = null;
        database db = database.getInstance();
        cnn=db.getConnection();
              try{
            String insertQuery = "INSERT INTO product(name,price) VALUES(?,?)";
            PreparedStatement statement = cnn.prepareStatement(insertQuery);
            Product product = new Product(-1,"Oh Leing",20);
            statement.setString(1, product.getName());
            statement.setDouble(2,product.getPrice());
            int row = statement.executeUpdate();
            ResultSet result = statement.getGeneratedKeys();
            int id=-1;
            if(result.next()){
                id = result.getInt(1);
            }System.out.println("Affect row " + row + " id "+id);
        }catch(SQLException ex){
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE,null,ex);
        }
        db.close();
    }
}