/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author Acer
 */
public class database {
    private static database instance = new database();
    private Connection cnn;
    private database() {
    }
    public static database getInstance() {
        String dbPath = "./db/coffee.db";
        try {
            if (instance.cnn == null || instance.cnn.isClosed()) {
                Class.forName("org.sqlite.JDBC");
                instance.cnn = DriverManager.getConnection("jdbc:sqlite:" + dbPath);
                System.out.println("Database connection");
            }
        } catch (ClassNotFoundException ex) {
            System.out.println("Error : JDCB is not exist");
        } catch (SQLException ex) {
            System.out.println("Error : Database cannot connection");
        }
        return instance;
    }
    
     public static void close() {
        try {
            if (instance.cnn != null || !instance.cnn.isClosed()) {
                instance.cnn.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(database.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        instance.cnn = null;
    }public Connection getConnection(){
        return instance.cnn;
    }


}